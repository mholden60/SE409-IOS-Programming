//
//  DetailViewController.swift
//  Homepwner
//
//  Created by Holden, Mathew E on 3/1/17.
//  Copyright © 2017 Big Nerd Ranch. All rights reserved.
//


import UIKit
class DetailViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var nameField: UITextField!

    @IBOutlet var serialNumberField: UITextField!

  
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @IBOutlet var valueField: UITextField!
    
    @IBOutlet var dateLabel: UILabel!
    
  var item: Item!
    {
    //*****************Keep Detail of item on Navbar*******************
    didSet {
        navigationItem.title = item.name
    }
    }
    //*****************************************************************
    let numberFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.minimumFractionDigits = 2
    formatter.maximumFractionDigits = 2
    return formatter
}()
    
let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .none
    return formatter
}()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameField.text = item.name
        serialNumberField.text = item.serialNumber
       // valueField.text = "\(item.valueInDollars)"
       // dateLabel.text = "\(item.dateCreated)"
        valueField.text =
        numberFormatter.string(from: NSNumber(value: item.valueInDollars))
    dateLabel.text = dateFormatter.string(from: item.dateCreated)
    }

    
    //*********************Hold Changes***********************************
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Clear first responder
    view.endEditing(true)
        
        // "Save" changes to item
        item.name = nameField.text ?? ""
        item.serialNumber = serialNumberField.text
        if let valueText = valueField.text,
            let value = numberFormatter.number(from: valueText) {
            item.valueInDollars = value.intValue
        } else {
            item.valueInDollars = 0
        }
    }
    //********************************************************************
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
return true }
}
